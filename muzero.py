# Lint as: python3
"""Pseudocode description of the MuZero algorithm."""
# pylint: disable=unused-argument
# pylint: disable=missing-docstring
# pylint: disable=assignment-from-no-return

from __future__ import absolute_import
from __future__ import division
from __future__ import print_function

import array
import collections
import math
import typing
from typing import Dict, List, Optional
import enum
import time
import os
from copy import copy, deepcopy
from os import path, rename, remove
from shutil import copyfile
import redis
import pickle
import glob
import subprocess
import traceback
import tempfile

from google.cloud import storage
import numpy
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
if os.environ.get('CPU'):
    device = 'cpu'
else:
    device = 'cuda:0' if torch.cuda.is_available() else 'cpu'

from tqdm import tqdm
import argparse

from mcts import Node
from utils import softmax_sample, select_child, visit_softmax_temperature, MinMaxStats, backpropagate

from network.network import (
    NetworkOutput,
    Conv,
    ResidualBlock,
    Representation,
    Prediction,
    Dynamics,
    Network,
)

#from pytorch_memlab import profile

##########################
####### Helpers ##########

MAXIMUM_FLOAT_VALUE = float('inf')

# noinspection PyArgumentList
Winner = enum.Enum("Winner", "black white draw")

num_filters = 128
num_blocks = 8

update_threshold = 0.55

games_per_generation = 5000

numpy.random.seed()

parser = argparse.ArgumentParser()
parser.add_argument('mode', type=str)

redis_client = redis.Redis(host=os.environ.get('REDIS', 'localhost'), port=6379, db=0)


class LookupTable(object):
    def __init__(self):
        self.table = {}

    def add(self, s, a, val):
        hash_key = f'{s.mean().item()}-{s.std().item()}-{a}'
        if hash_key in self.table:
            self.table[hash_key].append((s, val))
        else:
            self.table[hash_key] = [(s, val)]

    def get(self, s, a):
        hash_key = f'{s.mean().item()}-{s.std().item()}-{a}'
        if hash_key not in self.table:
            return None
        for d in self.table[hash_key]:
            if torch.eq(d[0], s):
                return d[1]
        return None


class MuZeroConfig(object):

    def __init__(self,
                 action_space_size: int,
                 max_moves: int,
                 discount: float,
                 dirichlet_alpha: float,
                 num_simulations: int,
                 batch_size: int,
                 td_steps: int,
                 num_actors: int,
                 lr_init: float,
                 lr_decay_steps: float,
                 known_bounds = None):
        ### Self-Play
        self.action_space_size = action_space_size
        self.num_actors = num_actors

        self.max_moves = max_moves
        self.num_simulations = num_simulations
        self.discount = discount

        # Root prior exploration noise.
        self.root_dirichlet_alpha = dirichlet_alpha
        self.root_exploration_fraction = 0.25

        # UCB formula
        self.pb_c_base = 19652
        self.pb_c_init = 1.25

        # If we already have some information about which values occur in the
        # environment, we can use them to initialize the rescaling.
        # This is not strictly necessary, but establishes identical behaviour to
        # AlphaZero in board games.
        self.known_bounds = known_bounds

        ### Training
        self.training_steps = int(1e6)
        self.checkpoint_interval = int(1000)
        self.window_size = int(1e5)
        self.batch_size = batch_size
        self.num_unroll_steps = 5
        self.td_steps = td_steps

        self.weight_decay = 1e-2
        self.momentum = 0.7

        # Exponential learning rate schedule
        self.lr_init = lr_init
        self.lr_decay_rate = 0.1
        self.lr_decay_steps = lr_decay_steps

    def new_game(self):
        return Game(self.action_space_size, self.discount)


def make_board_game_config(action_space_size: int, max_moves: int,
                           dirichlet_alpha: float,
                           lr_init: float) -> MuZeroConfig:

    return MuZeroConfig(
        action_space_size=action_space_size,
        max_moves=max_moves,
        discount=1.0,
        dirichlet_alpha=dirichlet_alpha,
        num_simulations=200,
        batch_size=int(1024),
        td_steps=max_moves,  # Always use Monte Carlo return.
        num_actors=2,
        lr_init=lr_init,
        lr_decay_steps=400e3,
        known_bounds=(-1, 1))

def make_connect4_config() -> MuZeroConfig:
    return make_board_game_config(
        action_space_size=7, max_moves=42, dirichlet_alpha=0.03,
        lr_init=0.02)


class ActionHistory(object):
    """Simple history container used inside the search.

    Only used to keep track of the actions executed.
    """

    def __init__(self, history: List[int], action_space_size: int):
        self.history = history
        self.action_space_size = action_space_size

    def clone(self):
        return ActionHistory(copy(self.history), self.action_space_size)

    def add_action(self, action: int):
        self.history.append(action)

    def last_action(self) -> int:
        return self.history[-1]

    def action_space(self) -> List[int]:
        return [i for i in range(self.action_space_size)]

    def to_play(self) -> int:
        if len(self.history) % 2 == 0:
            return 1
        else:
            return -1

class Environment(object):
    """The environment MuZero is interacting with."""

    def __init__(self):
        self.board = None
        self.turn = 0
        self.done = False
        self.winner = None  # type: Winner
        self.resigned = False

    def reset(self):
        self.board = []
        for i in range(6):
            self.board.append([])
            for j in range(7): # pylint: disable=unused-variable
                self.board[i].append(' ')
        self.turn = 0
        self.done = False
        self.winner = None
        self.resigned = False
        return self

    def update(self, board):
        self.board = numpy.copy(board)
        self.turn = self.turn_n()
        self.done = False
        self.winner = None
        self.resigned = False
        return self

    def turn_n(self):
        turn = 0
        for i in range(6):
            for j in range(7):
                if self.board[i][j] != ' ':
                    turn += 1

        return turn

    def player_turn(self):
        if self.turn % 2 == 0:
            return 1
        else:
            return -1

    def step(self, action):
        for i in range(6):
            if self.board[i][action] == ' ':
                self.board[i][action] = ('X' if self.player_turn() == 1 else 'O')
                break

        self.turn += 1

        self.check_for_fours()

        if self.turn >= 42:
            self.done = True
            if self.winner is None:
                self.winner = Winner.draw

        r = 0
        if self.done:
            if self.turn % 2 == 0:
                if Winner.white:
                    r = 1
                elif Winner.black:
                    r = -1
            else:
                if Winner.black:
                    r = 1
                elif Winner.white:
                    r = -1

        return r

    def legal_moves(self):
        legal = [0, 0, 0, 0, 0, 0, 0]
        for j in range(7):
            for i in range(6):
                if self.board[i][j] == ' ':
                    legal[j] = 1
                    break
        return legal

    def legal_actions(self):
        legal = []
        for j in range(7):
            for i in range(6):
                if self.board[i][j] == ' ':
                    legal.append(j)
                    break
        return legal

    def check_for_fours(self):
        for i in range(6):
            for j in range(7):
                if self.board[i][j] != ' ':
                    # check if a vertical four-in-a-row starts at (i, j)
                    if self.vertical_check(i, j):
                        self.done = True
                        return

                    # check if a horizontal four-in-a-row starts at (i, j)
                    if self.horizontal_check(i, j):
                        self.done = True
                        return

                    # check if a diagonal (either way) four-in-a-row starts at (i, j)
                    diag_fours = self.diagonal_check(i, j)
                    if diag_fours:
                        self.done = True
                        return

    def vertical_check(self, row, col):
        # print("checking vert")
        four_in_a_row = False
        consecutive_count = 0

        for i in range(row, 6):
            if self.board[i][col].lower() == self.board[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= 4:
            four_in_a_row = True
            if 'x' == self.board[row][col].lower():
                self.winner = Winner.white
            else:
                self.winner = Winner.black

        return four_in_a_row

    def horizontal_check(self, row, col):
        four_in_a_row = False
        consecutive_count = 0

        for j in range(col, 7):
            if self.board[row][j].lower() == self.board[row][col].lower():
                consecutive_count += 1
            else:
                break

        if consecutive_count >= 4:
            four_in_a_row = True
            if 'x' == self.board[row][col].lower():
                self.winner = Winner.white
            else:
                self.winner = Winner.black

        return four_in_a_row

    def diagonal_check(self, row, col):
        four_in_a_row = False
        count = 0

        consecutive_count = 0
        j = col
        for i in range(row, 6):
            if j > 6:
                break
            elif self.board[i][j].lower() == self.board[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= 4:
            count += 1
            if 'x' == self.board[row][col].lower():
                self.winner = Winner.white
            else:
                self.winner = Winner.black

        consecutive_count = 0
        j = col
        for i in range(row, -1, -1):
            if j > 6:
                break
            elif self.board[i][j].lower() == self.board[row][col].lower():
                consecutive_count += 1
            else:
                break
            j += 1

        if consecutive_count >= 4:
            count += 1
            if 'x' == self.board[row][col].lower():
                self.winner = Winner.white
            else:
                self.winner = Winner.black

        if count > 0:
            four_in_a_row = True

        return four_in_a_row

    def black_and_white_plane(self):
        board_white = numpy.zeros_like(self.board, dtype='i4')
        board_black = numpy.zeros_like(self.board, dtype='i4')
        for i in range(6):
            for j in range(7):
                if self.board[i][j] == ' ':
                    board_white[i][j] = 0
                    board_black[i][j] = 0
                elif self.board[i][j] == 'X':
                    board_white[i][j] = 1
                    board_black[i][j] = -1
                else:
                    board_white[i][j] = -1
                    board_black[i][j] = 1

        return board_white, board_black

    def render(self):
        print("\nRound: " + str(self.turn))

        for i in range(5, -1, -1):
            print("\t", end="")
            for j in range(7):
                print("| " + str(self.board[i][j]), end=" ")
            print("|")
        print("\t  _   _   _   _   _   _   _ ")
        print("\t  1   2   3   4   5   6   7 ")

        if self.done:
            print("Game Over!")
            if self.winner == Winner.white:
                print("X is the winner")
            elif self.winner == Winner.black:
                print("O is the winner")
            else:
                print("Game was a draw")

    @property
    def observation(self):
        return ''.join(''.join(x for x in y) for y in self.board)


class Game(object):
    """A single episode of interaction with the environment."""

    def __init__(self, action_space_size: int, discount: float):
        self.environment = Environment().reset()    # Game specific environment.
        self.history = []
        self.rewards = []
        self.child_visits = []
        self.root_values = []
        self.action_space_size = action_space_size
        self.discount = discount

    def terminal(self) -> bool:
        # Game specific termination rules.
        return self.environment.done

    def legal_actions(self) -> List[int]:
        # Game specific calculation of legal actions.
        return self.environment.legal_actions()

    def apply(self, action: int):
        reward = self.environment.step(action)
        reward = reward if self.environment.turn % 2 != 0 and reward == 1 else -reward
        self.rewards.append(reward)
        self.history.append(action)

    def store_search_statistics(self, root: Node):
        sum_visits = sum(child.visit_count for child in root.children.values())
        self.child_visits.append([
            root.children[a].visit_count / sum_visits if a in root.children else 0
            for a in range(self.action_space_size)
        ])
        self.root_values.append(root.value())
        #self.root_values.append(root.q)

    def make_image(self, state_index: int, player=None):
        # Game specific feature planes.
        o = Environment().reset()

        if state_index == -1:
            state_index = len(self.history)
        for current_index in range(0, state_index):
            o.step(self.history[current_index])

        black_ary, white_ary = o.black_and_white_plane()
        if player is None:
            state = [black_ary] if o.player_turn() == -1 else [white_ary]
        elif player == -1:
            state = [black_ary]
        elif player == 1:
            state = [white_ary]
        #state = [white_ary]
        return numpy.array(state)

    def make_target(self, state_index: int, num_unroll_steps: int, td_steps: int,
                    to_play: int):
        # The value target is the discounted root value of the search tree N steps
        # into the future, plus the discounted sum of all rewards until then.
        targets = []
        for current_index in range(state_index, state_index + num_unroll_steps + 1):
            if current_index < len(self.root_values):
                bootstrap_index = current_index + td_steps
                if bootstrap_index < len(self.root_values):
                    value = self.root_values[bootstrap_index] * self.discount**td_steps
                else:
                    value = 0

                for i, reward in enumerate(self.rewards[current_index:bootstrap_index]):
                    value += reward * self.discount**i  # pytype: disable=unsupported-operands

                if to_play == -1 and current_index % 2 == 0:
                    value = -value

                #print('state', current_index, value)
                targets.append((value, self.rewards[current_index],
                                self.child_visits[current_index]))
            else:
                # States past the end of games are treated as absorbing states.
                value = self.rewards[-1]
                if to_play == -1 and current_index % 2 == 0:
                    value = -value
                #print('absorb', current_index, value)
                targets.append((value, 0, [1 / self.action_space_size for _ in range(self.action_space_size)]))
        return targets

    def to_play(self) -> int:
        return self.environment.player_turn

    def action_history(self) -> ActionHistory:
        return ActionHistory(self.history, self.action_space_size)

class ReplayBuffer(object):

    def __init__(self, config: MuZeroConfig):
        self.window_size = config.window_size
        self.batch_size = config.batch_size
        #self.buffer = []
        self.redis = redis_client

    def save_game(self, game):
        self.redis.ltrim('buffer', 0, self.window_size)
        self.redis.lpush('buffer', pickle.dumps(game))

    def sample_batch(self, num_unroll_steps: int, td_steps: int):
        # Sample game from buffer either uniformly or according to some priority.
        game_idx = numpy.random.randint(self.redis.llen('buffer'), size=self.batch_size)
        games = [pickle.loads(self.redis.lindex('buffer', i.item())) for i in game_idx]
        game_pos = [(g, self.sample_position(g)) for g in games]

        batch_images = []
        batch_actions = []
        batch_target_values = [[]]
        batch_target_policy_logits = [[]]
        batch_target_rewards = [[]]
        for _ in range(num_unroll_steps):
            batch_actions.append([])
            batch_target_values.append([])
            batch_target_policy_logits.append([])
            batch_target_rewards.append([])
        for (g, i) in game_pos:
            batch_images.append(g.make_image(i))
            actions = g.history[i:i + num_unroll_steps]
            board = numpy.zeros((1, 6, 7))
            target = g.make_target(i, num_unroll_steps, td_steps, 1 if i % 2 == 0 else -1)
            batch_target_values[0].append(target[0][0])
            batch_target_rewards[0].append(target[0][1])
            batch_target_policy_logits[0].append(target[0][2])
            for i in range(num_unroll_steps):
                if i < len(actions):
                    action = actions[i]
                    board[:,:,action] = 1
                    batch_actions[i].append(board)
                else:
                    batch_actions[i].append(board)
                batch_target_values[i+1].append(target[i+1][0])
                batch_target_rewards[i+1].append(target[i+1][1])
                batch_target_policy_logits[i+1].append(target[i+1][2])
        batch_actions = numpy.array(batch_actions)

        assert len(batch_images) == self.batch_size

        return numpy.array(batch_images), batch_actions, (numpy.array(batch_target_values), numpy.array(batch_target_rewards), numpy.array(batch_target_policy_logits))

    def sample_position(self, game) -> int:
        # Sample position from game either uniformly or according to some priority.
        return numpy.random.choice(game.history)


class SharedStorage(object):

    def __init__(self):
        self.client = storage.Client()
        self.bucket = self.client.bucket('muzero-connect4')
        self.latest_steps = 0
        self.old_index = 0
        self.cached_network = None
        self.cached_network_steps = None

    def latest_network(self) -> Network:
        step = redis_client.get('latest_network').decode('utf-8')

        if self.cached_network_steps == step:
            return self.cached_network

        with tempfile.NamedTemporaryFile() as f:
            while True:
                try:
                    self.bucket.blob(f'network_{step}').download_to_filename(f.name)
                    break
                except Exception as e:
                    print(str(e))
                    time.sleep(5)
            network = torch.load(f.name, map_location=device)
        network.device = device
        network.representation.device = device
        network.prediction.device = device
        network.dynamics.device = device
        self.cached_network_steps = step
        self.cached_network = network
        return network

    def old_network(self) -> Network:
        raise Exception('Not implemented')
        network.device = device
        network.representation.device = device
        network.prediction.device = device
        network.dynamics.device = device
        return network

    def save_network(self, step: int, network: Network):
        self.latest_steps = step
        torch.save(network, 'latest')
        self.bucket.blob(f'network_{step}').upload_from_filename('latest')

    def accept_latest_network(self):
        raise Exception('Not implemented')

################################################################################
############################# Testing the latest net ###########################
################################################################################

def player_vs_older(old):
    old.eval()
    turn = True
    game = config.new_game()
    root = None
    current_root = None
    prev_action = None
    while not game.terminal():
        if turn:
            if not root:
                root = Node(0)
                current_root = root
            else:
                for a in list(current_root.children.keys()):
                    if a != prev_action:
                        del current_root.children[a]
                current_root = current_root.children[prev_action]
            current_observation = game.make_image(-1)
            inference = old.initial_inference(current_observation, False)
            print(current_observation)
            print('My valuation:', inference.value)
            print('Move policies:', inference.policy_logits)
            expand_node(current_root, game.to_play(), game.legal_actions(),
                        inference)
            run_mcts(config, current_root, game.action_history(), old)
            print('Visits:', [(a, c.visit_count) for a, c in current_root.children.items()])
            action = select_action(config, len(game.history), current_root, old, False)
        else:
            current_observation = game.make_image(-1)
            inference = old.initial_inference(current_observation, False)
            print(current_observation)
            print('Your valuation:', inference.value)
            game.environment.render()
            action = int(input('Your turn: ')) - 1
            prev_action = action
        game.apply(action)
        game.environment.render()
        turn = not turn
    current_observation = game.make_image(-1, 1)
    print(current_observation)
    inference = old.initial_inference(current_observation, False)
    print('Final valuation:', inference.value)
    current_observation = game.make_image(-1, -1)
    print(current_observation)
    inference = old.initial_inference(current_observation, False)
    print('Final valuation:', inference.value)


def latest_vs_older(last, old, n=100, debug=False):
    last.eval()
    old.eval()
    results = {}
    pbar = tqdm(desc='VS Older', total=n)
    action = None
    for i in range(n):
        first_turn = i % 2 == 0
        latest_root = latest_current_root = None
        oldest_root = oldest_current_root = None
        turn = first_turn
        game = config.new_game()
        r = 0
        while not game.terminal():
            if turn:
                if not latest_root:
                    latest_root = Node(0)
                    latest_current_root = latest_root
                else:
                    for a in list(latest_current_root.children.keys()):
                        if a != action:
                            del latest_current_root.children[a]
                    latest_current_root = latest_current_root.children[action]
                current_observation = game.make_image(-1)
                inference = last.initial_inference(current_observation, False)
                if debug:
                    print(inference.value, inference.policy_logits)
                expand_node(latest_current_root, game.to_play(), game.legal_actions(),
                            inference)
                #add_exploration_noise(config, root)
                run_mcts(config, latest_current_root, game.action_history(), last)
                action = select_action(config, len(game.history), latest_current_root, last, False, debug)
                if debug:
                    game.environment.render()
                    print(action)
            else:
                if not oldest_root:
                    oldest_root = Node(0)
                    oldest_current_root = oldest_root
                else:
                    for a in list(oldest_current_root.children.keys()):
                        if a != action:
                            del oldest_current_root.children[a]
                    oldest_current_root = oldest_current_root.children[action]
                current_observation = game.make_image(-1)
                expand_node(oldest_current_root, game.to_play(), game.legal_actions(),
                            old.initial_inference(current_observation, False))
                #add_exploration_noise(config, root)
                run_mcts(config, oldest_current_root, game.action_history(), old)
                action = select_action(config, len(game.history), oldest_current_root, old, False)
            game.apply(action)
            turn = not turn
            #game.environment.render()
        if ((game.environment.winner == Winner.white and first_turn)
                or (game.environment.winner == Winner.black and not first_turn)):
            r = 1
        elif ((game.environment.winner == Winner.black and first_turn)
              or (game.environment.winner == Winner.white and not first_turn)):
            r = -1
        results[r] = results.get(r, 0) + 1
        pbar.set_postfix(wins=results.get(1, 0), losses=results.get(-1, 0), draws=results.get(0, 0))
        pbar.update(1)
    pbar.close()
    return results

##### End Helpers ########
##########################


# MuZero training is split into two independent parts: Network training and
# self-play data generation.
# These two parts only communicate by transferring the latest network checkpoint
# from the training to the self-play, and the finished games from the self-play
# to the training.
def muzero(config: MuZeroConfig, mode: str):
    storage = SharedStorage()
    replay_buffer = ReplayBuffer(config)

    if mode == 'selfplay':
        run_selfplay(config, storage, replay_buffer)
    elif mode == 'selfplay_multi':
        processes = []
        for _ in range(10):
            processes.append(subprocess.Popen(['python', 'muzero.py', 'selfplay']))
        time.sleep(5)
        failed = False
        for p in processes:
            if p.poll() is not None:
                print('Self play exited!')
                failed = True
                break
        if failed:
            for p in processes:
                if p.poll() is None:
                    p.terminate()
            return

        try:
            while True:
                time.sleep(3600)
        except Exception:
            for p in processes:
                if p.poll() is None:
                    p.terminate()

    elif mode == 'uniform':
        network = make_uniform_network()
        torch.save(network, 'checkpoints/latest')
    elif mode == 'vs_older':
        storage.old_index = 23
        vs_older = latest_vs_older(storage.latest_network(), storage.old_network())
    elif mode == 'player':
        player_vs_older(storage.latest_network())
    elif mode == 'reset':
        network = make_uniform_network()
        redis_client.set('latest_network', 0)
        torch.save(network, 'latest')
        storage.bucket.blob(f'network_0').upload_from_filename('latest')
        replay_buffer.redis.delete('buffer')
    else:
        pbar = tqdm(desc='Self Play', total=games_per_generation*0.75)
        games = replay_buffer.redis.llen('buffer')
        if games < games_per_generation:
            print('Waiting for more games to be generated first...')
            prev_games = games
            pbar.update(games)
            while games < games_per_generation*0.75:
                time.sleep(1)
                games = replay_buffer.redis.llen('buffer')
                pbar.update(games - prev_games)
                prev_games = games
            pbar.close()
        train_network(config, storage, replay_buffer)


def current_gc_objects():
    import gc
    objs = 0
    for obj in gc.get_objects():
        try:
            if torch.is_tensor(obj) or (hasattr(obj, 'data') and torch.is_tensor(obj.data)):
                if obj.is_cuda:
                    #print(type(obj), obj.size())
                    #print(obj)
                    objs += 1
        except:
            pass
    print(objs)


##################################
####### Part 1: Self-Play ########


# Each self-play job is independent of all others; it takes the latest network
# snapshot, produces a game and makes it available to the training job by
# writing it to a shared replay buffer.
def run_selfplay(config: MuZeroConfig, storage: SharedStorage, replay_buffer: ReplayBuffer):
    if device != 'cpu':
        torch.backends.cudnn.benchmark = True
    with torch.no_grad():
        while True:
            network = storage.latest_network()
            network.eval()
            game = play_game(config, network)
            replay_buffer.save_game(game)


# Each game is produced by starting at the initial board position, then
# repeatedly executing a Monte Carlo Tree Search to generate moves until the end
# of the game is reached.
def play_game(config: MuZeroConfig, network: Network) -> Game:
    network = make_uniform_network()
    game = config.new_game()
    root = root_current = None
    root = Node(0)
    root_current = root
    action = None
    prev_root = None
    while not game.terminal() and len(game.history) < config.max_moves:
        # At the root of the search tree we use the representation function to
        # obtain a hidden state given the current observation.
        if action is not None:
            prev_root = root_current
            root_current = root_current.children[action]
        current_observation = game.make_image(-1)
        initial_inference = network.initial_inference(current_observation, False)
        legals = game.legal_actions()
        expand_node(root_current, game.to_play(), legals,
                    initial_inference)
        add_exploration_noise(config, root_current)

        prev_action = action

        # We then run a Monte Carlo Tree Search using only action sequences and the
        # model learned by the network.
        run_mcts(config, root_current, game.action_history(), network, True)
        #print('Policy for actions:', [(c.visit_count, a) for a, c in root_current.children.items()])
        action = select_action(config, len(game.history), root_current, network)
        #print('Selected action:', action)
        game.apply(action)
        #game.environment.render()
        game.store_search_statistics(root_current)
        if prev_action is not None and prev_root is not None:
            for a in list(prev_root.children.keys()):
                if a != prev_action:
                    del prev_root.children[a]
    if False:
        print(game.root_values)
        print(game.rewards)
        turn = int(max(0, len(game.root_values)-10))
        print(turn)
        #print(game.make_target(turn, 10, 42, Player.white if turn % 2 == 0 else Player.black))
        print(game.make_target(1, 5, 42, -1))
        import sys
        sys.exit(0)
    return game


# Core Monte Carlo Tree Search algorithm.
# To decide on an action, we run N simulations, always starting at the root of
# the search tree and traversing the tree according to the UCB formula until we
# reach a leaf node.
def run_mcts(config: MuZeroConfig, root: Node, action_history: ActionHistory,
             network: Network, training=False):
    min_max_stats = MinMaxStats(config.known_bounds[0], config.known_bounds[1])

    original_history = array.array('h', action_history.history)
    action_space = list(range(action_history.action_space_size))

    for _ in range(config.num_simulations):
        history = array.array('h', original_history)
        node = root
        search_path = [node]

        while node.expanded():
            action = select_child(config.pb_c_base, config.pb_c_init, node, min_max_stats)
            node = node.children[action]
            history.append(action)
            search_path.append(node)

        to_play = 1 if len(history) % 2 == 0 else -1

        # Inside the search tree we use the dynamics function to obtain the next
        # hidden state given an action and the previous hidden state.
        parent = search_path[-2]
        network_output = network.recurrent_inference(parent.hidden_state, board_actions[history[-1]], False)
        expand_node(node, to_play, action_space, network_output)

        backpropagate(search_path, network_output.value, config.discount, min_max_stats, to_play)


def select_action(config: MuZeroConfig, num_moves: int, node: Node,
                  network: Network, training=True, debug=False):
    visit_counts = []
    actions = []
    for action, child in node.children.items():
        visit_counts.append(child.visit_count)
        actions.append(action)
    if training:
        t = visit_softmax_temperature(
            num_moves=num_moves, training_steps=network.training_steps())
    else:
        t = 0
    action = softmax_sample(numpy.array(visit_counts, dtype=numpy.float), numpy.array(actions, dtype=numpy.int), t)
    return action


# Select the child with the highest UCB score.
'''
def select_child(config: MuZeroConfig, node: Node,
                 min_max_stats: MinMaxStats, debug=False):
    scores = [(ucb_score(config.pb_c_base, config.pb_c_init, node.visit_count, child.visit_count, child.value(), child.policy, min_max_stats), action,
               child) for action, child in node.children.items()]
    _, action, child = max(scores, key=lambda x: x[0])
    return action, child
'''


# We expand a node using the value, reward and policy prediction obtained from
# the neural network.
def expand_node(node: Node, to_play: int, actions: List[int],
                network_output: NetworkOutput):
    node.to_play = to_play
    node.hidden_state = network_output.hidden_state
    node.reward = network_output.reward
    if node.expanded():
        for a in list(node.children.keys()):
            if a not in actions:
                del node.children[a]
    else:
        for a in actions:
            node.children[a] = Node(network_output.policy_logits[a])


# At the start of each search, we add dirichlet noise to the policy of the root
# to encourage the search to explore new actions.
def add_exploration_noise(config: MuZeroConfig, node: Node):
    actions = list(node.children.keys())
    noise = numpy.random.dirichlet([config.root_dirichlet_alpha] * len(actions))
    frac = config.root_exploration_fraction
    for a, n in zip(actions, noise):
        node.children[a].policy = node.children[a].policy * (1 - frac) + n * frac


######### End Self-Play ##########
##################################

##################################
####### Part 2: Training #########
def train_network(config: MuZeroConfig, storage: SharedStorage,
                                    replay_buffer: ReplayBuffer):

    # network = Network(config.action_space_size).to(device)
    network = storage.latest_network()

    #optimizer = optim.SGD(network.parameters(), lr=config.lr_init, weight_decay=config.lr_decay_rate, momentum=config.momentum)
    #optimizer = optim.Adam(network.parameters(), weight_decay=config.lr_decay_rate)
    optimizer = optim.SGD(network.parameters(), lr=config.lr_init, weight_decay=config.weight_decay, momentum=config.momentum)

    '''
    pbar = tqdm(desc='Self Play', total=config.checkpoint_interval)
    network.eval()
    for _ in range(config.checkpoint_interval):
        game = play_game(config, network)
        replay_buffer.save_game(game)
        pbar.update(1)
        #return
    pbar.close()
    '''

    iters = 0
    print(f'--- GEN {iters+1} ---')
    pbar = tqdm(desc='Training', total=config.checkpoint_interval)
    p_loss_sum = v_loss_sum = 0

    i = 0

    while True:
        if i % config.checkpoint_interval == 0 and i > 0:
            pbar.close()
            storage.save_network(i, network)
            # Test against random agent
            #vs_random_once = vs_random(network)
            #print('network_vs_random = ', sorted(vs_random_once.items()))
            if i % (config.checkpoint_interval*10) == 0 and False:
                vs_older = latest_vs_older(storage.latest_network(), storage.old_network())
                print(f'lastnet_vs_older ({storage.old_index}) = ', sorted(vs_older.items()))
                if vs_older.get(1, 0) + vs_older.get(-1, 0) == 0 or float(vs_older.get(1, 0))/(vs_older.get(1, 0) + vs_older.get(-1, 0)) < update_threshold:
                    print('REJECTING NEW MODEL')
                else:
                    print('ACCEPTING NEW MODEL')
                    #storage.accept_latest_network()

            #storage.accept_latest_network()
            '''
            pbar = tqdm(desc='Self Play', total=config.checkpoint_interval)
            network.eval()
            for _ in range(config.checkpoint_interval):
                game = play_game(config, network)
                replay_buffer.save_game(game)
                pbar.update(1)
            pbar.close()
            '''
            games = replay_buffer.redis.llen('buffer')
            if games < ((i // config.checkpoint_interval) + 1) * games_per_generation:
                print('Waiting for more games to be generated first...')
                pbar = tqdm(desc='Self Play', total=((i // config.checkpoint_interval) + 1) * games_per_generation)
                prev_games = games
                pbar.update(games)
                while games < ((i // config.checkpoint_interval) + 1) * games_per_generation:
                    time.sleep(1)
                    games = replay_buffer.redis.llen('buffer')
                    pbar.update(games - prev_games)
                    prev_games = games
                pbar.close()

            iters += 1
            print(f'--- GEN {iters+1} ---')
            pbar = tqdm(desc='Training', total=config.checkpoint_interval)
        batch = replay_buffer.sample_batch(config.num_unroll_steps, config.td_steps)
        p_loss, v_loss = update_weights(batch, network, optimizer)
        if numpy.isnan(p_loss) or numpy.isnan(v_loss) or numpy.isinf(p_loss) or numpy.isinf(v_loss):
            raise Exception('Loss value went to NaN!')
        pbar.set_postfix(p_loss=f'{p_loss_sum/(i+1):0.3f}', v_loss=f'{v_loss_sum/(i+1):0.3f}', games=replay_buffer.redis.llen('buffer'))
        pbar.update(1)
        p_loss_sum += p_loss
        v_loss_sum += v_loss
        i += 1
    pbar.close()
    storage.save_network(i, network)


def update_weights(batch, network, optimizer):
    network.train()

    p_loss_sum = v_loss_sum = 0

    optimizer.zero_grad()
    batch_hidden_states, batch_policy_logits, batch_values = network.predict_initial_inference(batch[0], raw=True)
    target_policy = torch.Tensor(batch[2][2][0]).to(device).view(-1, config.action_space_size)
    target_values = torch.Tensor(batch[2][0][0]).to(device)
    p_loss = torch.mean(-torch.sum(target_policy * batch_policy_logits, dim=1)) / (config.num_unroll_steps + 1)
    v_loss = torch.mean((target_values - batch_values.view(-1)) ** 2) / (config.num_unroll_steps + 1)
    total_loss = p_loss + v_loss
    p_loss_sum += p_loss.item()
    v_loss_sum += v_loss.item()
    total_loss.backward(retain_graph=True)
    #predicted_values = [batch_values]
    #predicted_policy_logits = [batch_policy_logits]
    for i in range(config.num_unroll_steps):
        batch_hidden_states, batch_policy_logits, batch_values = network.predict_recurrent_inference(batch_hidden_states, torch.Tensor(batch[1][i]).to(device), raw=True)
        target_policy = torch.Tensor(batch[2][2][i]).to(device).view(-1, config.action_space_size)
        target_values = torch.Tensor(batch[2][0][i]).to(device)
        p_loss = torch.mean(-torch.sum(target_policy * batch_policy_logits, dim=1)) / (config.num_unroll_steps + 1)
        v_loss = torch.mean((target_values - batch_values.view(-1)) ** 2) / (config.num_unroll_steps + 1)
        total_loss = p_loss + v_loss
        p_loss_sum += p_loss.item()
        v_loss_sum += v_loss.item()
        total_loss.backward(retain_graph=i != config.num_unroll_steps - 1)
        network.dynamics.layer0.conv.weight.grad *= 0.5
        #predicted_values.append(batch_values)
        #predicted_policy_logits.append(batch_policy_logits)
    #predicted_values = torch.stack(predicted_values)
    #predicted_policy_logits = torch.stack(predicted_policy_logits).view(-1, config.action_space_size)
    #target_policy = torch.Tensor(batch[2][2]).to(device).view(-1, config.action_space_size)
    #target_values = torch.Tensor(batch[2][0]).to(device)
    #p_loss = -torch.sum(target_policy * predicted_policy_logits) / config.num_unroll_steps
    #v_loss = torch.sum((target_values - predicted_values.view(config.num_unroll_steps + 1, -1))**2) / (config.num_unroll_steps + 1)

    #optimizer.zero_grad()
    #total_loss = (p_loss + v_loss)
    #total_loss.backward()
    #print([t.grad for t in start_grads])
    #network.dynamics.layer0.conv.weight.grad *= 0.5
    #nn.utils.clip_grad_norm_(network.parameters(), 1.)
    #nn.utils.clip_grad_value_(network.parameters(), 0.5)
    optimizer.step()
    network.steps += 1

    return p_loss_sum, v_loss_sum


######### End Training ###########
##################################

################################################################################
############################# End of pseudocode ################################
################################################################################


def launch_job(f, *args):
    f(*args)


def make_uniform_network():
    return Network(make_connect4_config().action_space_size, num_filters, device).to(device)


config = make_connect4_config()

board_actions = []
for i in range(7):
    board = numpy.full((1, 1, 6, 7), 0)
    board[:,:,:,i] = 1
    board_actions.append(torch.Tensor(board).to(device))


def main():
    print(device)
    args = parser.parse_args()

    #vs_random_once = random_vs_random()
    #print('random_vs_random = ', sorted(vs_random_once.items()), end='\n')
    network = muzero(config, args.mode)


if __name__ == '__main__':
    main()
