import typing
from typing import Dict, List, Optional
import numpy
import torch
import torch.nn as nn
import torch.nn.functional as F

# Nets
class NetworkOutput(typing.NamedTuple):
    value: float
    reward: float
    policy_logits: Dict[int, float]
    hidden_state: List[float]


class Conv(nn.Module):
    def __init__(self, filters0, filters1, kernel_size, bn=False):
        super().__init__()
        self.conv = nn.Conv2d(filters0, filters1, kernel_size, stride=1, padding=kernel_size//2, bias=False)
        self.bn = None
        if bn:
            self.bn = nn.BatchNorm2d(filters1)

    def forward(self, x):
        h = self.conv(x)
        if self.bn is not None:
            h = self.bn(h)
        return h

class ResidualBlock(nn.Module):
    def __init__(self, filters):
        super().__init__()
        self.conv = Conv(filters, filters, 3, True)

    def forward(self, x):
        return F.relu(x + (self.conv(x)))

class Representation(nn.Module):
    ''' Conversion from observation to inner abstract state '''
    def __init__(self, input_shape, num_filters):
        super().__init__()
        self.input_shape = input_shape
        self.board_size = self.input_shape[1] * self.input_shape[2]

        self.layer0 = Conv(1, num_filters, 3, bn=True)
        self.blocks = nn.ModuleList([ResidualBlock(num_filters) for _ in range(8)])

    def forward(self, x):
        h = F.relu(self.layer0(x))
        for block in self.blocks:
            h = block(h)
        return h

class Prediction(nn.Module):
    ''' Policy and value prediction from inner abstract state '''
    def __init__(self, action_shape, num_filters):
        super().__init__()
        self.action_size = action_shape
        self.num_filters = num_filters

        self.layer0 = Conv(num_filters, 4, 3, bn=True)

        self.linear1 = nn.Linear(6 * 7 * 4, 512)
        self.linear1_bn = nn.BatchNorm1d(512)

        self.fc_p = nn.Linear(512, self.action_size)
        self.fc_v = nn.Linear(512, 1)

    def forward(self, rp, raw=False):
        rp_min = torch.min(torch.min(torch.min(rp, dim=3)[0], dim=2)[0], dim=1)[0]
        rp_max = torch.max(torch.max(torch.max(rp, dim=3)[0], dim=2)[0], dim=1)[0]
        rp_range = rp_max - rp_min
        rp_range[(rp_range < 1e-7) & (rp_range > -1e-7)] = 1
        rp_range = torch.unsqueeze(rp_range, -1)
        rp_range = torch.unsqueeze(rp_range, -1)
        rp_range = torch.unsqueeze(rp_range, -1)
        rp_range = rp_range.repeat(1, self.num_filters, 6, 7)
        rp_min = torch.unsqueeze(rp_min, -1)
        rp_min = torch.unsqueeze(rp_min, -1)
        rp_min = torch.unsqueeze(rp_min, -1)
        rp_min = rp_min.repeat(1, self.num_filters, 6, 7)
        rp = (rp - rp_min) / rp_range
        # rp = rp.view(-1, num_filters * 6 * 7)
        rp = F.relu(self.layer0(rp))
        rp = rp.view(-1, 6 * 7 * 4)
        rp = F.dropout(F.relu(self.linear1_bn(self.linear1(rp))), p=0.3, training=self.training)

        h_p = self.fc_p(rp)
        if raw:
            h_p = F.log_softmax(h_p, dim=-1)
        else:
            h_p = F.softmax(h_p, dim=-1)

        h_v = self.fc_v(rp)
        if False:
            print('h_p', h_p)
            print('h_v', h_v)
            print('rp1', rp.mean().item(), rp.std().item())
            print('weightv', self.fc_v.weight.mean().item(), self.fc_v.weight.std().item())

        # range of value is -1 ~ 1
        return h_p, torch.tanh(h_v)

class Dynamics(nn.Module):
    '''Abstruct state transition'''
    def __init__(self, rp_shape, act_shape, num_filters):
        super().__init__()
        self.rp_shape = rp_shape
        self.layer0 = Conv(rp_shape[0] + act_shape[0], num_filters, 3, bn=True)
        self.blocks = nn.ModuleList([ResidualBlock(num_filters) for _ in range(8)])

    def forward(self, rp, a):
        h = torch.cat([rp, a], dim=1)
        h = F.relu(self.layer0(h))
        for block in self.blocks:
            h = block(h)
        return h


class Network(nn.Module):
    def __init__(self, action_space_size: int, num_filters: int, device):
        super().__init__()
        self.steps = 0
        self.action_space_size = action_space_size
        self.device = device
        self.num_filters = num_filters
        input_shape = (num_filters, 6, 7)
        rp_shape = (num_filters, *input_shape[1:])
        self.representation = Representation(input_shape, num_filters).to(device)
        self.prediction = Prediction(action_space_size, num_filters).to(device)
        self.dynamics = Dynamics(rp_shape, (1, 6, 7), num_filters).to(device)
        self.eval()

    def predict_initial_inference(self, x, raw=False):
        assert x.ndim in (3, 4)
        assert x.shape == (1, 6, 7) or x.shape[1:] == (1, 6, 7)
        orig_x = x
        if x.ndim == 3:
            x = x.reshape(1, 1, 6, 7)

        x = torch.Tensor(x).to(self.device)
        h = self.representation(x)
        policy, value = self.prediction(h, raw)

        if orig_x.ndim == 3:
            return h[0], policy[0], value[0]
        else:
            return h, policy, value

    def predict_recurrent_inference(self, x, a, raw=False):
        orig_dim = x.ndim
        if x.ndim == 3:
            x = x.reshape(1, self.num_filters, 6, 7)

        g = self.dynamics(x, a)
        #print(g.mean(), g.std())
        policy, value = self.prediction(g, raw)

        if orig_dim == 3:
            return g[0], policy[0], value[0]
        else:
            return g, policy, value

    def initial_inference(self, image, training) -> NetworkOutput:
        # representation + prediction function
        h, p, v = self.predict_initial_inference(image.astype(numpy.float32))
        return NetworkOutput(v.item(), 0, p.tolist() if not training else p, h)

    def recurrent_inference(self, hidden_state, action, training) -> NetworkOutput:
        # dynamics + prediction function
        g, p, v = self.predict_recurrent_inference(hidden_state, action)
        return NetworkOutput(v.item(), 0, p.tolist() if not training else p, g)

    def training_steps(self) -> int:
        # How many steps / batches the network has been trained for.
        return self.steps

