import math
from cpython cimport array

from mcts import Node

import numpy


cdef class MinMaxStats:
    cdef double maximum, minimum

    def __init__(self, double minimum, double maximum):
        self.maximum = maximum
        self.minimum = minimum

    cdef update(self, double value):
        self.maximum = max(self.maximum, value)
        self.minimum = min(self.minimum, value)

    cdef double normalize(self, double value):
        if self.maximum > self.minimum:
            return (value - self.minimum) / (self.maximum - self.minimum)
        return value


cpdef long softmax_sample(double[:] visit_counts, long[:] actions, float temperature):
    cdef int max_idx
    if temperature == 0:
        max_idx = numpy.argmax(visit_counts)
        return actions[max_idx]
    distribution = numpy.array(visit_counts)**(1/temperature)
    p_sum = distribution.sum()
    sample_temp = distribution/p_sum
    max_idx = numpy.argmax(numpy.random.multinomial(1, sample_temp, 1))
    return actions[max_idx]


cpdef long select_child(double pb_c_base, double pb_c_init, object node, MinMaxStats min_max_stats):
    cdef long action, max_action
    cdef double score, max_score
    cdef object child
    max_score = -999
    max_action = 0
    for action, child in node.children.items():
        score = ucb_score(pb_c_base, pb_c_init, node.visit_count, child.visit_count, child.value(), child.policy, min_max_stats)
        if score > max_score:
            max_score = score
            max_action = action
    return max_action


cdef double ucb_score(double pb_c_base, double pb_c_init, long parent_visit_count, long child_visit_count, double child_value, double child_policy, MinMaxStats min_max_stats):
    cdef double pb_c, policy_score
    pb_c = math.log((parent_visit_count + pb_c_base + 1) / pb_c_base) + pb_c_init
    pb_c *= math.sqrt(parent_visit_count) / (child_visit_count + 1)

    policy_score = pb_c * child_policy
    return policy_score + min_max_stats.normalize(child_value)


cpdef double visit_softmax_temperature(long num_moves, long training_steps):
    if num_moves < 20:
        return 0
    return ((20. - num_moves) / 20.) * (1. - 0.3) + 0.3


cpdef backpropagate(list search_path, double value, double discount, MinMaxStats min_max_stats, long to_play):
    for node in search_path:
        assert isinstance(node, Node)
        node.value_sum += value if node.to_play == to_play else -value
        node.visit_count += 1
        min_max_stats.update(node.value())

        value = node.reward + discount * value
